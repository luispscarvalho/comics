import React from 'react';
import { StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Splash from './src/screens/Splash';
import Display from './src/screens/Display';

import {
  Background
} from './src/style';

function SplashScreen({ navigation }) {
  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={Background.color} />
      <Splash navigation={navigation} />
    </>
  );
}

function DisplayScreen({ navigation }) {
  return (
    <>
      <StatusBar hidden={true} />
      <Display navigation={navigation} />
    </>
  );
}

const RootStack = createStackNavigator();
function App() {
  return (
    <NavigationContainer>
      <RootStack.Navigator mode="modal" headerMode="none">
        <RootStack.Screen name="Main" component={SplashScreen} />
        <RootStack.Screen name="Display" component={DisplayScreen} />
      </RootStack.Navigator>
    </NavigationContainer>
  );
}

export default App;