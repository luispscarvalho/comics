import styled from 'styled-components/native';

export const Title = styled.Text`
    color: #694a38;
    font-size: 38
`;

export const Info = styled.Text`
    color: #694a38;
    font-size: 17
`;

export const Padder = styled.View`
    padding: 10px;
    flexDirection: row;
`;

export const PageContainer = styled.TouchableOpacity`
    flex: 1;
    flex-direction: row;
    justify-content: center;
    align-items: stretch;
    resize-mode: stretch;
    width: 100%;
    height: 100%;
 }`;


export const Page = styled.Image`
    flex: 1;
    resize-mode: stretch;
    width: 100%;
    height: 100%;
 }`;

 export const Dialog = styled.Image`
    flex: 1;
    resize-mode: stretch;
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
 }`;

 

export const Background = {
    color: '#f4ac45'
}

export const FloatingButton = {
    color: '#a61c3c', 
    size: 36, 

    iconColor: '#a61c3c', 
    iconSize: 50
}