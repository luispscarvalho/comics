import React from 'react';
import { Image, TouchableOpacity } from 'react-native';

import Tap from '../../components/Tap';
import {
    Title,
    Info,
    Padder,

    Background
} from '../../style';
import info from '../../assets/info.json';

/**
 * This class is responsible for showing the splash 
 * screen
 * 
 * It is a very simple plain screen that can be used
 * to show basic information about the app. Ideally, 
 * any new information must be inserted in the 
 * '../../assets/info.json' file and referenced 
 * by the render() method.
 * 
 * TODO: perhaps, preloading the images is a good
 * move and this can be done while the splash screen
 * is being shown.
 */
export default class Splash extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            navigation: this.props.navigation
        }
    }

    render() {
        const { navigation } = this.state;

        return (
            <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: Background.color }} onPress={() => navigation.navigate('Display')}>
                <Image source={require('../../assets/images/logo.png')} />
                <Title>{info.title}</Title>
                <Padder />
                <Info>a different comic book experience by</Info>
                <Info>{info.author}</Info>
                <Info>Version: {info.version}</Info>
                <Padder />
                <Padder />
                <Info>tap to reveal the dialogs</Info>
                <Tap />
            </TouchableOpacity>
        );
    }
}
