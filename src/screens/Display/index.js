import React from 'react';
import { FloatingAction } from 'react-native-floating-action';

import {
    Page,
    PageContainer,
    Dialog,

    FloatingButton
} from '../../style';
import Pages from '../../components/Pages';

/**
 * This class is responsible for showing all comic's pages 
 * and dialogs.
 * 
 * The pages are bundled together with the application
 * and accessed as static images. The collection of pages &
 * dialogs are loaded from the Pages components (in 
 * '../../components/Pages'). WARNING: there's 
 * seemingly a limit to the number of images that 
 * can be statically stored in an app. I couldn't
 * find any reliable reference about it, but it must tested
 * if it is possible to embed all comic's pages and dialogs
 * in the app and deploy it to both android and iPhone.
 */
export default class Display extends React.Component {

    /**
     * This is the first routine executed by
     * react-native. It creates the state with all
     * necessary properties to exhibit pages and 
     * dialogs.
     * 
     * @param {*} props 
     */
    constructor(props) {
        super(props);

        this.state = {
            navigation: this.props.navigation,

            pages: [],
            currentPage: 0,
            currentDialog: -1,

            loaded: false
        }

    }

    componentDidMount() {
        this.load();
    }


    /**
     * Loads all pages, as configured in '../../components/Pages', it
     * is important to set 'loaded' to true only after the pages (and dialogs) 
     * have been loaded.
     * 
     * Preferably, the "setState" instruction must always be 
     * the last one, because it updates the screen. 
     */
    load() {
        const pages = Pages();

        this.setState({ pages: pages, loaded: true });
    }

    /**
     * This method is reponsible for showing
     * the background of the page and its dialogs. 
     * 
     * It is going to be executed after the user interacts
     * with the app and the 'nextDialog()', 'nextPage()'
     * and 'previousPage()' methods are called. 
     */
    showNext() {
        const { pages, currentPage, currentDialog } = this.state;

        const page = pages[currentPage];
        const dialog = page.dialogs[currentDialog];

        return (
            <>
                <Page source={page.image} />
                {(currentDialog >= 0) && <Dialog source={dialog} />}
            </>);
    }

    /**
     * This method is responsible for showing the next 
     * dialog as long as there are dialogs left. After
     * showing the last dialog, it will return to the 
     * first one if the user keeps on tapping. 
     * 
     * TODO: the routine is not ideal, because it requires
     * the user to perceive that the first dialog was shown
     * again to finally advance to the next page. How to
     * warn the user that he is seing the last dialog?
     */
    nextDialog() {
        const { pages, currentPage, currentDialog } = this.state;
        const page = pages[currentPage];

        // are there any dialogs left?
        let nextDialog = currentDialog;
        if (nextDialog < page.dialogs.length) {
            // increments the index to show the next one
            nextDialog = nextDialog + 1;
        } else {
            // otherwise, let's show the first dialog again
            nextDialog = 0;
        }

        // updates the state and refreshes the screen
        this.setState({
            currentDialog: nextDialog
        });
    }

    /**
     * Shows the next page. The "setState" instruction
     * triggers the transition, "currentDialog" is set
     * to -1 to start showing dialogs from the first 
     * one in the showNext() method
     */
    nextPage() {
        const { pages, currentPage } = this.state;

        let nextPage = currentPage;
        if (nextPage < pages.length) {
            nextPage = nextPage + 1;
        }

        this.setState({
            currentPage: nextPage,
            currentDialog: -1
        })

    }

    /**
     * shows the previous page. If it reaches the first
     * page of the comic book, it goes back to the 
     * splash screen. The "currentDialog" variable is set
     * to -1 to start showing dialogs from the first 
     * one in the showNext() method
     */
    previousPage() {
        const { navigation, currentPage } = this.state;

        let previousPage = currentPage;
        if (previousPage > 0) {
            previousPage = previousPage - 1;
        } else {
            navigation.goBack();
        }

        this.setState({
            currentPage: previousPage,
            currentDialog: -1
        })
    }

    /**
     * these are the actions (round buttons) showed
     * at the bottom right conner of the screen.
     * 
     * TODO: add a "share" action/button so that
     * the reader may invite other people to 
     * download the app. 
     */
    actions() {
        const actions = [
            {
                icon: require("../../assets/images/next.png"),
                name: "next",
                color: FloatingButton.iconColor,
                buttonSize: FloatingButton.iconSize,
                position: 1
            },
            {
                icon: require("../../assets/images/back.png"),
                name: "previous",
                color: FloatingButton.iconColor,
                buttonSize: FloatingButton.iconSize,
                position: 1
            }
        ];

        return actions;
    }

    /**
     * This method is responsible for rendering
     * pages, dialogs and the floating buttons
     * that show next and previous pages/dialogs
     */
    render() {
        const { loaded } = this.state;

        // let's prevent rendering if the pages have not been loaded yet
        if (!loaded) {
            return (null);
        }

        // shows everything - the { this.nextDialog() } below is activated by taps
        // {this.showNext()} exhibits current page and its dialogs
        return (
            <PageContainer style={{ flex: 1, alignItems: 'center', justifyContent: 'center', height: '100%', width: '100%' }}
                onPress={() => { this.nextDialog() }}> 
                {this.showNext()}

                <FloatingAction color={FloatingButton.color} distanceToEdge={15} buttonSize={FloatingButton.size} iconWidth={15} iconHeight={15}
                    actions={this.actions()}
                    onPressItem={name => {
                        if (name === 'next') {
                            this.nextPage();
                        } else if (name === 'previous') {
                            this.previousPage();
                        }
                    }} />
            </PageContainer>
        );
    }
}
