import React from 'react';
import { Image } from 'react-native';

/**
 * from https://www.skptricks.com/2019/03/add-blinking-animation-on-text-in-react-native-app.html
 */
export default class Tap extends React.Component {
    constructor(props) {
        super(props);
        this.state = { show: true };

        setInterval(() => {
            this.setState(previousState => {
                return { show: !previousState.show };
            });
        }, 1600);
    }

    render() {
        const icon = (this.state.show ?
            <>
                <Image style={{ width: 40, height: 40 }} source={require('../../assets/images/dialogs.png')} />
                <Image style={{ width: 30, height: 30 }} source={require('../../assets/images/tap.png')} />
            </> :
            <>
                <Image style={{ width: 40, height: 40 }} source={require('../../assets/images/blankdialogs.png')} />
                <Image style={{ width: 30, height: 30 }} source={require('../../assets/images/untap.png')} />
            </>);

        return (<>{icon}</>);
    }
}
