/**
 * This function is responsible for listing all
 * pages and dialogs image files after they 
 * are stored in the 'assets/comic' folder
 * 
 * It is very important that the files are named
 * in a manner to reflect the dependency between
 * pages and dialogs. Just follow the format below:
 * 
 * Page X main file: "X.png". The sequence of 
 * dialog files must be named as: "X-1.png",
 * "X-2.png" ... "X-n.png".
 */
const Pages = () => {
    const pages = [
        {
            image: require('../../assets/comic/1.png'),
            dialogs: [
                require('../../assets/comic/1-1.png'), 
                require('../../assets/comic/1-2.png'), 
                require('../../assets/comic/1-3.png'), 
                require('../../assets/comic/1-4.png')
            ]
        },
        {
            image: require('../../assets/comic/2.png'),
            dialogs: [
                require('../../assets/comic/2-1.png'), 
                require('../../assets/comic/2-2.png')
            ]
        }
    ];

    return pages;
}

export default Pages;